#include <iostream>
#include "Matchfield.h"

int main()
{
    //Init
    Matchfield matchfield;
    matchfield.outputMatchfield();

    //Run to Ball
    matchfield.movePlayerTowardsBall();

    //Shoot
    std::cout << std::endl << "Schuss!" << std::endl;
    matchfield.moveBallTowardsGoal();
    std::cout << std::endl << "Tor!" << std::endl;

    return 0;
}
