
#include <cmath>
#include <random>
#include "Matchfield.h"

#define MAX_FIELD_SIZE 15
#define MIN_FIELD_SIZE 10

#define MIN_GOAL_LEN 2

//elementwise access: field_m(y,x) , y is vertical axis, x is horizontal axis
Matchfield::Matchfield()
:length_m(0),
 width_m(0),
 goal_m(Goal(0,DOWN)),
 player_m(Player(0,0)),
 ball_m(Ball(0,0)),
 field_m(Matrix(0,0,'0'))
{
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()

    std::uniform_int_distribution<size_t> distrField(MIN_FIELD_SIZE, MAX_FIELD_SIZE); //Includes the bounds
    length_m = distrField(gen);
    width_m =  distrField(gen);

    std::cout << "length_m: " << length_m <<std::endl;
    std::cout << "width_m: " << width_m <<std::endl;

    std::uniform_int_distribution<size_t> distrPositionX(1, width_m-2); //dont want to have anything on the border
    std::uniform_int_distribution<size_t> distrPositionY(1, length_m-2);

    std::uniform_int_distribution<size_t> distrGoalBorder(0, 3);

    size_t borderChoice = distrGoalBorder(gen);
    if(borderChoice == 0)
        goal_m.setPlace(UP);
    if(borderChoice == 1)
        goal_m.setPlace(DOWN);
    if(borderChoice == 2)
        goal_m.setPlace(LEFT);
    if(borderChoice == 3)
        goal_m.setPlace(RIGHT);

    if(goal_m.getPlace() == UP || goal_m.getPlace() == DOWN)
    { // MIN_GOAL_LEN, width_m-2
        std::uniform_int_distribution<size_t> distrGoalLen(MIN_GOAL_LEN,width_m-3);
        goal_m.setLength( distrGoalLen(gen) );
    }

    if(goal_m.getPlace() == LEFT || goal_m.getPlace() == RIGHT)
    { // MIN_GOAL_LEN, width_m-2
        std::uniform_int_distribution<size_t> distrGoalLen(MIN_GOAL_LEN,length_m-3);
        goal_m.setLength( distrGoalLen(gen) );
    }

    std::cout << "goal_m.getLength() " << goal_m.getLength() <<std::endl;

    Matrix field(length_m,width_m,'_');
    field_m = field;
    Player player((size_t) distrPositionX(gen), (size_t) distrPositionY(gen));
    player_m = player;

    Ball ball((size_t) distrPositionX(gen), (size_t) distrPositionY(gen));
    ball_m = ball;

    std::cout << "player_m.getPositionX() " << player_m.getPositionX() <<std::endl;
    std::cout << "player_m.getPositionY() " << player_m.getPositionY() <<std::endl;
    std::cout << "ball_m.getPositionX() " << ball_m.getPositionX() <<std::endl;
    std::cout << "ball_m.getPositionY() " << ball_m.getPositionY() <<std::endl;
    std::cout << "field_m.cols() " << field_m.cols() <<std::endl;
    std::cout << "field_m.rows() " << field_m.rows() <<std::endl << std::endl;

    //set them on the field
    field_m = goal_m.initGoal(field_m);
    field_m = player_m.initPlayer(field_m);
    field_m = ball_m.initBall(field_m);

}

const void Matchfield::movePlayerTowardsBall()
{
    size_t distanceX = (size_t) std::abs((int)ball_m.getPositionX() - (int)player_m.getPositionX());
    size_t distanceY = (size_t) std::abs((int)ball_m.getPositionY()- (int)player_m.getPositionY());

    size_t currentPlayerPositionX = player_m.getPositionX();
    size_t currentPlayerPositionY = player_m.getPositionY();

    while(!(distanceX == 0 && distanceY == 0))
    {
        bool stepTodo = false;
        //horizontal step
        if(ball_m.getPositionX() > player_m.getPositionX())
        {
            std::cout << std::endl << "Bewege mich nach rechts!" << std::endl;
            currentPlayerPositionX++;
            stepTodo = true;
        }

        if(ball_m.getPositionX() < player_m.getPositionX())
        {
            std::cout << std::endl << "Bewege mich nach links!" << std::endl;
            currentPlayerPositionX--;
            stepTodo = true;
        }

        if(stepTodo) //execute horizontal step
        {
            field_m = player_m.movePlayerToPosition(field_m,ball_m,currentPlayerPositionX,currentPlayerPositionY); //reset char at old position and move ball to new position
            outputMatchfield();
            stepTodo = false; //reset
        }


        //vertical step
        if(ball_m.getPositionY() > player_m.getPositionY())
        {
            std::cout << std::endl << "Bewege mich nach unten!" << std::endl;
            currentPlayerPositionY++;
            stepTodo = true;
        }

        if(ball_m.getPositionY() < player_m.getPositionY())
        {
            std::cout << std::endl << "Bewege mich nach oben!" << std::endl;
            currentPlayerPositionY--;
            stepTodo = true;
        }

        if(stepTodo) //execute horizontal step
        {
            field_m = player_m.movePlayerToPosition(field_m,ball_m,currentPlayerPositionX,currentPlayerPositionY); //reset char at old position and move ball to new position
            outputMatchfield();
            //stepTodo = false; //reset
        }

        //refresh distances
        distanceX = (size_t) std::abs((int)ball_m.getPositionX() - (int)player_m.getPositionX());
        distanceY = (size_t) std::abs((int)ball_m.getPositionY() - (int)player_m.getPositionY());

        std::cout << "distanceX: " << distanceX << std::endl;
        std::cout << "distanceY: " << distanceY << std::endl;
    }

}



const void Matchfield::moveBallTowardsGoal()
{
    int targetPositionX=0,targetPositionY=0;

    //place the target in front the goal
    if(goal_m.getPlace() == UP)
    {
        targetPositionX = (int)goal_m.getPositionX();
        targetPositionY = (int)goal_m.getPositionY() + 1;
    }

    if(goal_m.getPlace()== DOWN)
    {
        targetPositionX = (int)goal_m.getPositionX();
        targetPositionY = (int)goal_m.getPositionY() - 1;
    }

    if(goal_m.getPlace() == LEFT)
    {
        targetPositionX = (int)goal_m.getPositionX() + 1;
        targetPositionY = (int)goal_m.getPositionY();
    }

    if(goal_m.getPlace() == RIGHT)
    {
        targetPositionX = (int)goal_m.getPositionX() - 1;
        targetPositionY = (int)goal_m.getPositionY();
    }




    ball_m.setAtPlayer(true);

    size_t distanceX = (size_t) std::abs((int)ball_m.getPositionX() - targetPositionX);
    size_t distanceY = (size_t) std::abs((int)ball_m.getPositionY() - targetPositionY);

    size_t currentBallPositionX = ball_m.getPositionX();
    size_t currentBallPositionY = ball_m.getPositionY();

    while(!(distanceX == 0 && distanceY == 0))
    {
        bool stepTodo = false;
        //horizontal step
        if(ball_m.getPositionX() < targetPositionX)
        {
            currentBallPositionX++;
            stepTodo = true;
        }

        if(ball_m.getPositionX() > targetPositionX)
        {
            currentBallPositionX--;
            stepTodo = true;
        }

        if(stepTodo) //execute horizontal step
        {
            field_m = ball_m.moveBallToPosition(field_m,currentBallPositionX,currentBallPositionY); //reset char at old position and move ball to new position
            outputMatchfield();
            stepTodo = false; //reset
        }


        //vertical step
        if(ball_m.getPositionY() < targetPositionY)
        {
            currentBallPositionY++;
            stepTodo = true;
        }

        if(ball_m.getPositionY() > targetPositionY)
        {
            currentBallPositionY--;
            stepTodo = true;
        }

        if(stepTodo) //execute horizontal step
        {
            field_m = ball_m.moveBallToPosition(field_m,currentBallPositionX,currentBallPositionY); //reset char at old position and move ball to new position
            outputMatchfield();
            //stepTodo = false; //reset
        }

        //refresh distances
        distanceX = (size_t) std::abs((int)ball_m.getPositionX() - targetPositionX);
        distanceY = (size_t) std::abs((int)ball_m.getPositionY() - targetPositionY);

        std::cout << "distanceX: " << distanceX << std::endl;
        std::cout << "distanceY: " << distanceY << std::endl;
    }

}



const void Matchfield::outputMatchfield()
{
    field_m.outputMatrix();
}