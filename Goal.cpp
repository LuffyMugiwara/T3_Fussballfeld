#include <cmath>
#include "Goal.h"

Goal::Goal(const size_t length, const Direction place)
        :place_m(place),
         length_m(length)
{


}


Matrix Goal::initGoal(Matrix field)
{
    const size_t fieldLength = field.rows();
    const size_t fieldWidth = field.cols();


    //Position always in the middle of the border
    if(place_m == UP || place_m == DOWN)
    {
        //the very left element
        positionX_m = (size_t) std::floor(fieldWidth / 2 - length_m / 2);

        if(place_m == UP)
            positionY_m = 0;
        if(place_m == DOWN)
            positionY_m = fieldLength-1;

        for(size_t i=0; i < length_m; i++)
            field(positionY_m,positionX_m+i) = '#';
    }

    if(place_m == LEFT || place_m == RIGHT)
    {
        positionY_m = (size_t) std::floor(fieldLength / 2 - length_m / 2);

        if(place_m == LEFT)
            positionX_m = 0;
        if(place_m == RIGHT)
            positionX_m = fieldWidth-1;

        for(size_t i=0; i < length_m; i++)
            field(positionY_m+i,positionX_m) = '#';
    }

    return field;
}



//Getter
Direction Goal::getPlace() const
{
    return place_m;
}
size_t Goal::getLength() const
{
    return length_m;
}

size_t Goal::getPositionX() const
{
    return positionX_m;
}

size_t Goal::getPositionY() const
{
    return positionY_m;
}

//Setter
const void Goal::setPlace(const Direction place)
{
    place_m = place;
}

const void Goal::setLength(const size_t length)
{
    length_m = length;
}

//const void Goal::setPositionX(const size_t positionX)
//{
//    positionX_m = positionX;
//}
//const void Goal::setPositionY(const size_t positionY)
//{
//    positionY_m = positionY;
//}