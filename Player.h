

#ifndef T3_FUSSBALLFELD_PLAYER_H
#define T3_FUSSBALLFELD_PLAYER_H

#include "Matrix.h"
#include "Ball.h"

class Player {

    // - - - member variables - - - - - - - - - - - - - - - - - - - - - - - - - - -
public:


private:
    size_t positionX_m;
    size_t positionY_m;

    // - - - methods  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
public:
    Player(const size_t positionX, const size_t positionY);
    Matrix initPlayer(Matrix field) const;
    Matrix movePlayerToPosition(Matrix field, const Ball& ball, const size_t positionX, const size_t positionY);

    //Getter
    size_t getPositionX() const;
    size_t getPositionY() const;

    //Setter
//    const void setPositionX(const size_t position_X);
//    const void setPositionY(const size_t position_Y);



private:


};


#endif //T3_FUSSBALLFELD_PLAYER_H
