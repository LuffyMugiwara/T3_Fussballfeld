
#ifndef T3_FUSSBALLFELD_GOAL_H
#define T3_FUSSBALLFELD_GOAL_H

#include "Matrix.h"
#include "Tools.h"

class Goal {

    // - - - member variables - - - - - - - - - - - - - - - - - - - - - - - - - - -
public:


private:
    Direction place_m;
    size_t length_m;
    size_t positionX_m;
    size_t positionY_m;

    // - - - methods  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
public:
    Goal(const size_t length, const Direction place);
    Matrix initGoal(Matrix field);

    //Getter
    Direction getPlace() const;
    size_t getLength() const;
    size_t getPositionX() const;
    size_t getPositionY() const;

    //Setter
    const void setPlace(const Direction place);
    const void setLength(const size_t length);
//    const void setPositionX(const size_t position_X);
//    const void setPositionY(const size_t position_Y);

private:


};


#endif //T3_FUSSBALLFELD_GOAL_H
