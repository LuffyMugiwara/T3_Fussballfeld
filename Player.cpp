
#include "Player.h"


Player::Player(const size_t positionX, const size_t positionY)
:positionX_m(positionX),
 positionY_m(positionY)
{

}

Matrix Player::initPlayer(Matrix field) const
{
    field(positionY_m,positionX_m) = '+';
    return field;
}

Matrix Player::movePlayerToPosition(Matrix field, const Ball& ball, const size_t positionX, const size_t positionY)
{
    field(positionY_m,positionX_m) = '_'; //reset current

    positionY_m = positionY;
    positionX_m = positionX;

    if(ball.getPositionX() == positionX_m && ball.getPositionY() == positionY_m)
        field(positionY_m,positionX_m) = '!';
    else
        field(positionY_m,positionX_m) = '+';

    return field;
}



//Getter
size_t Player::getPositionX() const
{
    return positionX_m;
}

size_t Player::getPositionY() const
{
    return positionY_m;
}


////Setter
//const void Player::setPositionX(const size_t positionX)
//{
//    positionX_m = positionX;
//}
//const void Player::setPositionY(const size_t positionY)
//{
//    positionY_m = positionY;
//}