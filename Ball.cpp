
#include "Ball.h"


Ball::Ball(const size_t positionX, const size_t positionY)
:positionX_m(positionX),
 positionY_m(positionY)
{
    atPlayer_m = false;
}

Matrix Ball::initBall(Matrix field) const
{
    field(positionY_m,positionX_m) = '*';
    return field;
}


Matrix Ball::moveBallToPosition(Matrix field, const size_t positionX, const size_t positionY)
{
    if(atPlayer_m)
        field(positionY_m,positionX_m) = '+'; //replace ! with +
    else
        field(positionY_m,positionX_m) = '_'; //reset current

    positionY_m = positionY;
    positionX_m = positionX;

    field(positionY_m,positionX_m) = '*';

    atPlayer_m = false; //ball left the player now

    return field;
}



//Getter
size_t Ball::getPositionX() const
{
    return positionX_m;
}

size_t Ball::getPositionY() const
{
    return positionY_m;
}

//const bool Ball::getAtPlayer() const
//{
//    return atPlayer_m;
//}


//Setter
//const void Ball::setPositionX(const size_t positionX)
//{
//    positionX_m = positionX;
//}
//const void Ball::setPositionY(const size_t positionY)
//{
//    positionY_m = positionY;
//}

const void Ball::setAtPlayer(const bool playerAt)
{
    atPlayer_m = playerAt;
}