

#ifndef T3_FUSSBALLFELD_MATRIX_H
#define T3_FUSSBALLFELD_MATRIX_H


#include <iostream>
#include <vector>

class Matrix
{
    // - - - type definitions - - - - - - - - - - - - - - - - - - - - - - - - - - -

public:
    typedef std::vector<std::vector<char>> data_t;

    // - - - member variables - - - - - - - - - - - - - - - - - - - - - - - - - - -
public:


private:
    data_t data_m;
    size_t rows_m;
    size_t cols_m;

    // - - - methods  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
public:
    Matrix(const size_t rows, const size_t cols, const char val);

    const void outputMatrix() const;

    const size_t rows() const;
    const size_t cols() const;


    char& operator() (const size_t row_a, const size_t col_a) ;
    const char& operator() (const size_t row_a, const size_t col_a) const;

private:


};


#endif //T3_FUSSBALLFELD_MATRIX_H
