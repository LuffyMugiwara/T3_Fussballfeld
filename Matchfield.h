
#ifndef T3_FUSSBALLFELD_SPIELFELD_H
#define T3_FUSSBALLFELD_SPIELFELD_H

#include <iostream>
#include "Matrix.h"
#include "Goal.h"
#include "Ball.h"
#include "Player.h"


class Matchfield {

    //public:


    // - - - member variables - - - - - - - - - - - - - - - - - - - - - - - - - - -
    public:

    private:
        size_t length_m;
        size_t width_m;
        Matrix field_m;
        Player player_m;
        Goal goal_m;
        Ball ball_m;



    // - - - methods  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    public:
        Matchfield();
        const void outputMatchfield();
        const void movePlayerTowardsBall();
        const void moveBallTowardsGoal();

    private:
        //void initGoal(size_t length, Direction place);


};


#endif //T3_FUSSBALLFELD_SPIELFELD_H
