
#ifndef T3_FUSSBALLFELD_BALL_H
#define T3_FUSSBALLFELD_BALL_H

#include "Matrix.h"

class Ball {

    // - - - member variables - - - - - - - - - - - - - - - - - - - - - - - - - - -
public:


private:
    size_t positionX_m;
    size_t positionY_m;
    bool atPlayer_m;

    // - - - methods  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
public:
    Ball(const size_t positionX, const size_t positionY);
    Matrix initBall(Matrix field) const;
    Matrix moveBallToPosition(Matrix field, size_t positionX, size_t positionY);

    //Getter
    size_t getPositionX() const;
    size_t getPositionY() const;
//    const bool getAtPlayer() const;

    //Setter
//    const void setPositionX(const size_t position_X);
//    const void setPositionY(const size_t position_Y);
    const void setAtPlayer(const bool atPlayer);

private:


};


#endif //T3_FUSSBALLFELD_BALL_H
